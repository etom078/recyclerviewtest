package com.example.recyclerviewtest

data class Card(
    val title: String,
    val desc: String,
    val size: Int,
    val type: Int
) {
    companion object {
        val VIEW_TYPE_EMPTY: Int = 0
        val VIEW_TYPE_CARD: Int = 1
    }
}
