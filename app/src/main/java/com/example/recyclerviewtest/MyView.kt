package com.example.recyclerviewtest

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.drawable.Drawable
import android.text.TextPaint
import android.util.AttributeSet
import android.view.View

/**
 * TODO: document your custom view class.
 */
class MyView : View {

    constructor(context: Context) : super(context) {
        init(null, 0)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        init(attrs, defStyle)
    }

    private fun init(attrs: AttributeSet?, defStyle: Int) {

    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        var paint: Paint = Paint()
        paint.style = Paint.Style.FILL_AND_STROKE
        paint.strokeWidth = 5F
        paint.color = Color.rgb(0, 0, 0)
        var x: Float = canvas.width / 2F
        canvas.drawLine(x, 10F, x, canvas.height.toFloat() - 10F, paint)
        canvas.drawLine(x - 50F, canvas.height.toFloat() - 10F - 50F, x, canvas.height.toFloat() - 10F, paint)
        canvas.drawLine(x, canvas.height.toFloat() - 10F, x + 50F, canvas.height.toFloat() - 10F - 50F, paint)
    }
}