package com.example.recyclerviewtest

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

class MyAdapter : ListAdapter<Card, MyAdapter.MyViewHolder>(CardsComparator()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder.create(parent, viewType)
    }

    override fun getItemViewType(position: Int): Int {
        return getItem(position).type
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val current = getItem(position)
        holder.bind(current)
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(card: Card) {
            if (card.type == Card.VIEW_TYPE_EMPTY) {
                val view : View = itemView.findViewById(R.id.emptyView)
                val params : ViewGroup.LayoutParams = view.layoutParams
                params.height = card.size
                view.layoutParams = params
            } else {
                val titleTextView: TextView = itemView.findViewById<TextView>(R.id.textView1)
                val descTextView: TextView = itemView.findViewById<TextView>(R.id.textView2)
                val cardView: CardView = itemView.findViewById<CardView>(R.id.cardView)
                titleTextView.text = card.title
                descTextView.text = card.desc
                val params: ViewGroup.LayoutParams = cardView.layoutParams
                params.height = card.size
                cardView.layoutParams = params
            }
        }

        companion object {
            fun create(parent: ViewGroup, viewType: Int) : MyViewHolder {
                val view: View = LayoutInflater.from(parent.context)
                    .inflate(when(viewType) {
                        Card.VIEW_TYPE_EMPTY -> R.layout.list_item_empty
                        Card.VIEW_TYPE_CARD -> R.layout.list_item
                        else ->  R.layout.list_item
                    }, parent, false)
                return MyViewHolder(view)
            }
        }
    }

    class CardsComparator : DiffUtil.ItemCallback<Card>() {
        override fun areItemsTheSame(oldItem: Card, newItem: Card): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: Card, newItem: Card): Boolean {
            return (oldItem.title == newItem.title) && (oldItem.desc == newItem.desc)
        }
    }
}