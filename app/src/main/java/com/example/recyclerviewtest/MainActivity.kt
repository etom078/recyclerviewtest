package com.example.recyclerviewtest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val recyclerView = findViewById<RecyclerView>(R.id.recyclerview)
        val adapter = MyAdapter()
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)

        var cards = arrayListOf<Card>(
            Card("Test1", "test 1", 400, Card.VIEW_TYPE_CARD),
            Card("", "", 200, Card.VIEW_TYPE_EMPTY),
            Card("Test2", "test 2", 200, Card.VIEW_TYPE_CARD),
            Card("", "", 400, Card.VIEW_TYPE_EMPTY),
            Card("Test3", "test 3", 300, Card.VIEW_TYPE_CARD))
        adapter.submitList(cards)
    }
}